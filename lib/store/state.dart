import 'package:get/get.dart';
import 'package:workplace_booking_app/data/booking.dart';
import 'package:workplace_booking_app/data/workplace.dart';

final List<Workplace> workplaces = <Workplace>[].obs;
final List<Booking> historyBookings = <Booking>[].obs;