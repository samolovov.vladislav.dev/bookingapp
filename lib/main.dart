import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:workplace_booking_app/data/workplace.dart';
import 'package:workplace_booking_app/store/state.dart';
import 'package:workplace_booking_app/tabs_menu_view.dart';
import 'package:get/get.dart';
import 'package:xml_parser/xml_parser.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  application();
}

application() async {
  rootBundle.loadString("assets/workspace.svg").then((value) {
    XmlDocument? xmlDocument = XmlDocument.from(value);

    final paths = xmlDocument!.getElementsWhere(name: 'path');

    if (paths != null) {
      workplaces.addAll(paths
          .where((element) {
            String? className;

            try {
              className = element.attributes!.firstWhere((attribute) => attribute.name == 'class').value;
            } catch (e) {}

            return className == 'workplace';
          })
          .map((path) => Workplace.fromXml(path))
          .toList());
    }
  });

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'GT Walsheim Pro',
          textTheme: const TextTheme(bodyText2: TextStyle(color: Color(0xFF1D1D1D))),
          appBarTheme: const AppBarTheme(
              backgroundColor: Color(0xFF1D1D1D), titleTextStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
        ),
        home: const TabsMenuView());
  }
}
