import 'package:flutter/material.dart';
import 'package:workplace_booking_app/ui/booking_history_view.dart';
import 'package:workplace_booking_app/ui/workspaces_list_view.dart';


class TabsMenuView extends StatefulWidget {
  const TabsMenuView({Key? key}) : super(key: key);

  @override
  State<TabsMenuView> createState() => _TabsMenuViewState();
}

class _TabsMenuViewState extends State<TabsMenuView> with SingleTickerProviderStateMixin {
  static const List<Tab> tabs = <Tab>[
    Tab(icon: Icon(Icons.home_outlined)),
    Tab(icon: Icon(Icons.schedule_outlined)),
  ];

  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: tabs.length);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: TabBarView(
          controller: tabController,
          children: [
            WorkspacesListView(goToHistory: () {
              setState(() {
                tabController.index = 1;
              });
            }),
            const BookingHistoryView(),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
            child: TabBar(
              controller: tabController,
              labelColor: const Color(0xFF1D1D1D),
              indicator: const BoxDecoration(
                border: Border(top: BorderSide(color: Color(0xFF1D1D1D), width: 3)),
              ),
              tabs: tabs,
            )));
  }
}