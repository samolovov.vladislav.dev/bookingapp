import 'package:flutter/material.dart';
import 'package:workplace_booking_app/store/state.dart';

class BookingHistoryView extends StatelessWidget {
  const BookingHistoryView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('История бронирований'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 30),
        children: [
          ...historyBookings.map((booking) {
            return Container(
              margin: const EdgeInsets.only(bottom: 15),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: [BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.2), blurRadius: 10)]),
              padding: const EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(children: [
                    Container(
                      child: const Icon(
                        Icons.autorenew,
                        size: 14,
                        color: Colors.white,
                      ),
                      height: 30,
                      width: 30,
                      margin: const EdgeInsets.only(right: 15),
                      decoration: const BoxDecoration(
                        color: Color(0xFF3E3EBA),
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        '#${booking.id}',
                        style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text('${booking.timestamp.hour}:${booking.timestamp.minute}:${booking.timestamp.second}',
                          style: const TextStyle(fontSize: 15, color: Color(0xFFAEAFB7))),
                    )
                  ]),
                  const Padding(
                    padding: EdgeInsets.only(top: 15),
                    child: Text('Кузнецкий Мост #303', style: TextStyle(fontSize: 18)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15, bottom: 30),
                    child: Text('Место: ${booking.workplace.id}', style: const TextStyle(fontSize: 18)),
                  )
                ],
              ),
            );
          })
        ],
      ),
    );
  }
}
