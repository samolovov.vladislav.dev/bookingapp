import 'package:flutter/material.dart';

import 'package:workplace_booking_app/data/workplace.dart';
import 'package:workplace_booking_app/ui/workspace/path_painter.dart';

class WorkplaceModel {
  Workplace workplace;
  final GlobalKey key = GlobalKey();

  WorkplaceModel(this.workplace);
}

class WorkplaceWidget extends StatelessWidget {
  final WorkplaceModel model;

  const WorkplaceWidget(this.model, {Key? key}) : super(key: key);

  Color get color {
    if (model.workplace.isSelected) {
      return const Color(0xFF17916B);
    }

    return model.workplace.isFree ? const Color(0xFF3E3EBA) : const Color(0xFFAEAFB7);
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
        key: model.key,
        child: GestureDetector(),
        painter: PathPainter(model.workplace.path, color));
  }
}
