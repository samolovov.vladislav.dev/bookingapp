import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:workplace_booking_app/data/workplace.dart';
import 'package:workplace_booking_app/ui/workspace/path_painter.dart';
import 'package:workplace_booking_app/ui/workspace/workplace_widget.dart';
import 'package:xml_parser/xml_parser.dart';
import 'package:svg_path_parser/svg_path_parser.dart';

class WorkspaceWidget extends StatefulWidget {
  final String pathToSvgAsset;
  final List<Workplace> workplaces;
  final void Function(Workplace workplace)? onTap;

  const WorkspaceWidget(this.pathToSvgAsset, {required this.workplaces, this.onTap, Key? key}) : super(key: key);

  @override
  State<WorkspaceWidget> createState() => _WorkspaceWidgetState();
}

class _WorkspaceWidgetState extends State<WorkspaceWidget> {
  late List<List<dynamic>> svgPaths = [];

  late List<WorkplaceModel> workplacesModels = [];

  late double svgHeight = 100;
  late double svgWidth = 100;

  updateWorkplaces () {
    setState(() {
      workplacesModels = widget.workplaces.map((workplace) => WorkplaceModel(workplace)).toList();
    });
  }

  @override
  void initState() {
    super.initState();

    rootBundle.loadString(widget.pathToSvgAsset).then((value) {
      XmlDocument? xmlDocument = XmlDocument.from(value);

      XmlElement? svgContainer = xmlDocument!.getElement('svg');

      double height = double.parse(svgContainer!.attributes!.firstWhere((attribute) => attribute.name == 'height').value);
      double width = double.parse(svgContainer.attributes!.firstWhere((attribute) => attribute.name == 'width').value);

      final paths = xmlDocument.getElementsWhere(name: 'path');

      if (paths != null) {
        setState(() {
          svgHeight = height;
          svgWidth = width;

          svgPaths = paths.where((element) {
            String? className;

            try {
              className = element.attributes!.firstWhere((attribute) => attribute.name == 'class').value;
            } catch (e) {}

            return className != 'workplace';
          }).map((path) {
            String d = path.attributes!.firstWhere((attribute) => attribute.name == 'd').value;

            String hexColor = path.attributes!.firstWhere((attribute) => attribute.name == 'fill').value;
            hexColor = hexColor.replaceAll("#", "");

            return [parseSvgPath(d), Color(int.parse("0xFF$hexColor"))];
          }).toList();

          workplacesModels = widget.workplaces.map((workplace) => WorkplaceModel(workplace)).toList();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerUp: (PointerEvent pointerEvent) {
        WorkplaceModel? selectedWorkplaceModel;

        for (var workplaceModel in workplacesModels) {
          final RenderBox? workplaceWidgetBox = workplaceModel.key.currentContext!.findRenderObject() as RenderBox?;
          Offset localClick = workplaceWidgetBox!.globalToLocal(pointerEvent.position);

          if (workplaceWidgetBox.hitTest(BoxHitTestResult(), position: localClick)) {
            selectedWorkplaceModel = workplaceModel;
          }
        }

        if (selectedWorkplaceModel != null && widget.onTap != null) {
          widget.onTap!(selectedWorkplaceModel.workplace);
        }
      },
      child: SizedBox(
        height: svgHeight,
        width: svgWidth,
        child: Stack(
          children: [
            ...svgPaths.map((e) {
              return CustomPaint(
                  painter: PathPainter(
                      e[0] as Path, e[1] as Color));
            }).toList(),
            ...workplacesModels.map((model) => WorkplaceWidget(model)).toList()
          ],
        ),
      ),
    );
  }
}
