import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:workplace_booking_app/data/booking.dart';
import 'package:workplace_booking_app/data/workplace.dart';
import 'package:workplace_booking_app/store/state.dart';
import 'package:workplace_booking_app/ui/workspace/workspace_widget.dart';

class WorkspaceView extends StatefulWidget {
  final Function? onComplete;

  const WorkspaceView({this.onComplete, Key? key}) : super(key: key);

  @override
  State<WorkspaceView> createState() => _WorkspaceViewState();
}

class _WorkspaceViewState extends State<WorkspaceView> {
  Workplace? selectedWorkplace;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: workplaces.isNotEmpty
              ? WorkspaceWidget("assets/workspace.svg", workplaces: workplaces, onTap: (workplace) {
                  if (workplace.isFree) {
                    if (!workplace.isSelected) {
                      setState(() {
                        selectedWorkplace?.isSelected = false;
                        selectedWorkplace = workplace;
                        workplace.isSelected = true;
                      });
                    } else {
                      setState(() {
                        selectedWorkplace = null;
                        workplace.isSelected = false;
                      });
                    }
                  }
                })
              : const SizedBox.shrink(),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              height: 80,
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: ElevatedButton(
                  onPressed: selectedWorkplace != null
                      ? () async {
                          setState(() {
                            selectedWorkplace!.isFree = false;
                            selectedWorkplace!.isSelected = false;
                          });
                          historyBookings.add(Booking(selectedWorkplace!));
                          bool? isComplete = await showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return SimpleDialog(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(14.0),
                                  ),
                                  contentPadding: const EdgeInsets.all(0),
                                  titlePadding: const EdgeInsets.all(0),
                                  insetPadding: const EdgeInsets.all(0),
                                  title: const Padding(
                                    padding: EdgeInsets.all(15.0),
                                    child: Text('Бронирование успешно!',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18)),
                                  ),
                                  children: <Widget>[
                                    SimpleDialogOption(
                                      padding: const EdgeInsets.all(0),
                                      onPressed: () {
                                        Navigator.pop(context, false);
                                      },
                                      child: Container(
                                          decoration: const BoxDecoration(
                                            border: Border.symmetric(
                                              horizontal: BorderSide(
                                                color: Color(0xFFEAEAEA),
                                                width: 1,
                                              ),
                                            ),
                                          ),
                                          child: const Padding(
                                            padding: EdgeInsets.all(15.0),
                                            child: Text('Забронировать еще',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(color: Color(0xFF3E3EBA), fontSize: 18)),
                                          )),
                                    ),
                                    SimpleDialogOption(
                                      padding: const EdgeInsets.all(0),
                                      onPressed: () {
                                        Navigator.pop(context, true);
                                      },
                                      child: const Padding(
                                        padding: EdgeInsets.all(15.0),
                                        child: Text('Завершить',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(color: Color(0xFF3E3EBA), fontSize: 18)),
                                      ),
                                    ),
                                  ],
                                );
                              });
                          if (isComplete == true && widget.onComplete != null) {
                            widget.onComplete!();
                          }
                        }
                      : null,
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                          selectedWorkplace == null ? const Color(0xFF717171) : const Color(0xFF1D1D1D)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      )),
                  child: Text(selectedWorkplace == null ? 'Укажите место на карте...' : 'Подтвердить',
                      style: const TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w700)),
                ),
              ),
            ))
      ],
    );
  }
}
