import 'package:flutter/material.dart';
import 'package:workplace_booking_app/ui/workspace/workspace_view.dart';

class WorkspacesListView extends StatefulWidget {
  final Function? goToHistory;
  const WorkspacesListView({this.goToHistory, Key? key}) : super(key: key);

  @override
  State<WorkspacesListView> createState() => _WorkspacesListViewState();
}

class _WorkspacesListViewState extends State<WorkspacesListView> {
  bool isListMode = true;

  Widget buildListView () {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Бронирование места в офисе'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 30),
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                isListMode = false;
              });
            },
            child: Container(
              margin: const EdgeInsets.only(bottom: 15),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: [BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.2), blurRadius: 10)]),
              padding: const EdgeInsets.all(15),
              child: const Text(
                'Кузнецкий Мост #303',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildWorkspaceView () {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.chevron_left),
          onPressed: () {
            setState(() {
              isListMode = true;
            });
          },
        ),
        title: const Text('#303 Кузнецкий Мост'),
      ),
      body: WorkspaceView(onComplete: () {
        if (widget.goToHistory != null) {
          widget.goToHistory!();
        }
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return isListMode ? buildListView() : buildWorkspaceView();
  }
}
