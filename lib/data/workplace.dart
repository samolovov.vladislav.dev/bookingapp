import 'dart:ui';

import 'package:xml_parser/xml_parser.dart';
import 'package:svg_path_parser/svg_path_parser.dart';

class Workplace {
  late int id;
  late bool isFree;
  late bool isSelected;
  late Path path;

  Workplace.fromXml(XmlElement pathElement) {
    path = parseSvgPath(pathElement.attributes!.firstWhere((attribute) => attribute.name == 'd').value);
    id = int.parse(pathElement.attributes!.firstWhere((attribute) => attribute.name == 'data-id').value);
    isFree = pathElement.attributes!.firstWhere((attribute) => attribute.name == 'data-is-free').value == 'true';
    isSelected = false;
  }
}