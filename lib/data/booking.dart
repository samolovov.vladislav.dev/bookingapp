import 'dart:ui';

import 'package:workplace_booking_app/data/workplace.dart';
import 'package:xml_parser/xml_parser.dart';
import 'package:svg_path_parser/svg_path_parser.dart';

class Booking {
  late final int id;
  late final DateTime timestamp;
  final Workplace workplace;

  Booking(this.workplace) {
    timestamp = DateTime.now();
    id = int.parse('${timestamp.microsecond}');
  }
}